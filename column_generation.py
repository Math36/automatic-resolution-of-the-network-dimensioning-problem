#!/usr/bin/env python
# coding: utf-8

import networkx as nx
from networkx.generators.lattice import *
from networkx.generators.random_graphs import *
from networkx.linalg.graphmatrix import adjacency_matrix
from networkx.algorithms.shortest_paths.generic import shortest_path
from networkx.generators import *

import numpy as np

from pulp import *
"""
We use pulp to solve the problem

please vheck if it is installed
otherwise use pip install pulp
"""
solver = getSolver('PULP_CBC_CMD')

def generate_graph(graph_name, m=None, n=None, p=None):
    """    
    Create a Manhattan graph or an erdos renyi graph with random cost for each edge
    
    Arguments:
    	graph_name {string} -- 'grid_2d_graph' for Manhattan or 'erdos_renyi_graph'
    
    Keyword Arguments:
    	m {integer} -- number of nodes in height of the Manhattan graph (default: {None})
    	n {integer} -- number of nodes in widht for the Manhattan graph
    				   or the number of nodes of the Erdos renyi graph (default: {None})
    	p {float} -- [description] (default: {None})
    
    Returns:
    	graph -- the graph
    """
    if graph_name=="grid_2d_graph":
        g = grid_2d_graph(m, n)
    if graph_name=="erdos_renyi_graph":
        g = erdos_renyi_graph(n, p, seed=None, directed=False)
    nx.set_edge_attributes(g, values=0, name='capacity')
    nx.set_edge_attributes(g, values=0, name='cost')
    # create random cost
    for edge in g.edges:
        nx.set_edge_attributes(g, {edge: {'cost':np.random.randint(1,5)}})
    return g

def generate_shortest_paths(graph, sources, targets):
    """
    Parameters:
        graph: The graph on which we want to compute the initial paths
        sources: The list of source nodes for our paths
        targets: The list of target nodes for our paths
        
    Return:
        The list of the shortest initial paths
    """
    paths = list()
    if len(sources)==len(targets):
        for i in range(len(sources)):
            paths.append(shortest_path(graph, sources[i], targets[i], 'capacity'))
    return paths

def print_one_path(graph, path):
	"""

	Show the graph with a specific path highlighted

	Arguments:
	graph  -- the graph
	path {list} -- the path which be highlighted
	"""
	pos = nx.spring_layout(graph)

	path_edges = list(zip(path,path[1:]))

	# Draw nodes and edges not included in path
	nx.draw_networkx_nodes(graph, pos, nodelist=set(graph.nodes)-set(path))
	nx.draw_networkx_edges(graph, pos, edgelist=set(graph.edges)-set(path_edges), connectionstyle='arc3, rad = 0.3')

	# Draw nodes and edges included in path
	nx.draw_networkx_nodes(graph, pos, nodelist=path, node_color='r')
	nx.draw_networkx_edges(graph,pos,edgelist=path_edges,edge_color='r', connectionstyle='arc3, rad = 0.3')

	# Draw labels
	nx.draw_networkx_labels(graph,pos)
    
def print_whole_graph(graph):
	"""
	print the graph

	Arguments:
		graph
	"""
	pos = nx.spring_layout(graph)

	nx.draw(graph, pos)
	node_labels = nx.get_node_attributes(graph,'capacity')
	nx.draw_networkx_labels(graph, pos, labels = node_labels)
	edge_labels = nx.get_edge_attributes(graph,'capacity')
	nx.draw_networkx_edge_labels(graph, pos, edge_labels = edge_labels)
	nx.draw_networkx(graph, pos=pos, font_size=10, node_color='white', font_color='black')
    
def edge_in_path(path, edge):
	"""

	Check if an edge is in a path

	Arguments:
		path {list} -- the path
		edge {list} -- the edge

	Returns:
		bool {boolean} -- return True if the edge is in the path, otherwise False
	"""
	for i in range(len(path)-1):
	    if (path[i], path[i+1]) == edge or (path[i], path[i+1]) == (edge[1], edge[0]):
	        return True
	return False

def create_master_problem(graph, instance_paths=None, demand_values=None):
	"""

	create and solve the problem given a graph, paths and a deman

	Arguments:
		graph {[type]} -- The graph on which we compute the solution

	Keyword Arguments:
		instance_paths {dict} -- Dict of paths for each demands for which we compute the solution (default: {None})
		demand_values {list} -- List of the demand's values (default: {None})

	Returns:
		graph, prob -- The updated graph and the problem
	"""

	#Generate the capacities variables
	capacities = dict()
	cost = dict()
	i=0
	for node1, node2, data in graph.edges(data=True):
	    capacities[(node1,node2)]=LpVariable(f'y{i}',0, cat="Integer")
	    cost[(node1,node2)]=data["cost"]
	    i+=1


	#Generate the instance_paths variables
	demand2flows = dict() #permet de retrouver pour chaque demande, la liste de flots associée
	flow2paths = dict() #Permet de retrouver le chemin associé à ce flot
	path2flow = dict() #Permet de retrouver pour chaque chemin le flot associé

	for _, paths in instance_paths.items():
	    for path in paths:
	        path2flow[str(path)] = list()
	i=0
	for demand, paths in instance_paths.items():
	    demand2flows[demand] = []
	    j=0
	    for path in paths:
	        temp = LpVariable(f'x{i}{j}',0, cat="Integer")
	        demand2flows[demand].append(temp)
	        flow2paths[f'x{i}{j}'] = path
	        path2flow[str(path)].append(temp)
	        j+=1
	    i+=1
	    
	#Create the problem
	prob = LpProblem("Master_problem", LpMinimize)

	prob += sum([capacities[k] * cost[k] for k in capacities]) #Objective function

	# Constraints
	# First constraint
	for i in range(len(demand_values)):
	    prob += sum(demand2flows[f'demand_{i}']) >= demand_values[i]
	    
	# Second constraint
	for edge, capacity in capacities.items():
	    temp_list = list()
	    for flow, path in flow2paths.items():
	        if not(flow in [str(elmt) for elmt in temp_list]):
	            if edge_in_path(path, edge):
	                for elmt in path2flow[str(path)]:
	                    temp_list.append(elmt)
	    if temp_list:
	        prob += sum(temp_list) <= capacity


	prob.solve()
	new_graph = update_graph(graph, capacities)
	return new_graph, prob

def update_graph(graph, new_capacities):
	"""

	Update the graph capacities with the new capacities

	Arguments:
		graph -- the graph
		new_capacities {dict} -- dictionnary with edges associate with their capacity

	Returns:
		the graph updated
	"""
	for edge, capacity in new_capacities.items():
	    nx.set_edge_attributes(graph, {edge: {'capacity':capacity.varValue}})
	return graph

def generate_demand(size, lowBound, upBound):
	"""

	generate random demand

	Arguments:
		size {integer} -- number of demand
		lowBound {integer} -- lower bound
		upBound {integer} -- upper bound

	Returns:
		List of demand
	"""
	return [np.random.randint(lowBound, upBound) for i in range(size)]

def generate_source_target(nbdemand, graph):
	"""	
	generate sources and targets following the number of demand

	Arguments:
		nbdemand {integer} -- the number of demand
		graph  -- the graph

	Returns:
		List, List -- two lists (sources and corresponding targets)
	"""
	source = list()
	target = list()
	nodes = [node for node in graph.nodes]
	nb = nbdemand
	while nb > 0:
	    found = False
	    while not(found):
	        s = nodes[np.random.randint(0,len(nodes))]
	        t = nodes[np.random.randint(0,len(nodes))]
	        # check if there is a path between source and target in the graph
	        if s != t and nx.algorithms.shortest_paths.generic.has_path(graph,s,t):
	            source.append(s)
	            target.append(t)
	            found = True
	            nb -= 1
	return source, target